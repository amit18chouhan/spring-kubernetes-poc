package com.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class Controller {

    Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public ResponseEntity function(@PathVariable String name) {
        LOGGER.info("Rest Controller Invoked : values : {}", name);
        return ResponseEntity.status(HttpStatus.OK).body("Hello " + name);
    }

    @RequestMapping(value = "/{firstName}/{lastName}", method = RequestMethod.GET)
    public ResponseEntity function(@PathVariable String firstName, @PathVariable String lastName) {
        LOGGER.info("Rest Controller Invoked : values : {} {}", firstName, lastName);
        return ResponseEntity.status(HttpStatus.OK).body("Hello " + firstName + " " + lastName);
    }
}